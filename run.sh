#!/bin/bash
MYSQL_CONTAINER="${1:-mysql}"

#RAM="$(free -om | tail -2 | head -1 | awk '{print $2}')"
RAM="$(free|awk '/^Mem:/{print $2}')"
#SWAP="$(free -om | tail -1 | awk '{print $2}')"
SWAP="$(free|awk '/^Swap:/{print $2}')"

docker run -ti \
  --rm \
  --link "${MYSQL_CONTAINER}:mysql" \
  -v "${PWD}:/data" \
  katta/mysqltuner --forcemem "$RAM" --forceswap "$SWAP"
