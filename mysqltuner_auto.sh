#!/bin/bash
# TODO: generate a .my.cfg file instead of appends arguments to cmd
ARGS=""

if [ -n "$MYSQL_PORT_3306_TCP_ADDR" ] ; then
  ARGS="$ARGS --host $MYSQL_PORT_3306_TCP_ADDR"
fi

if [ -n "$MYSQL_PORT_3306_TCP_PORT" ] ; then
  ARGS="$ARGS --port $MYSQL_PORT_3306_TCP_PORT"
fi


if [ -n "$MYSQL_ENV_MYSQL_ROOT_PASSWORD" ] ; then
  ARGS="$ARGS --user root --pass $MYSQL_ENV_MYSQL_ROOT_PASSWORD"
fi

echo "mysqltuner $ARGS $*"
# shellcheck disable=SC2086,SC2048
/usr/local/bin/mysqltuner ${ARGS} $*
